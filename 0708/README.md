JavierSese-Project

learned and practiced thins in class

1- Repository JavierSese-Project created with GitLab

2- Repository cloned in local with Kraken

3- SASS and Compass installation
	From terminal:
	
	- sudo apt-get install ruby-full  //ruby install 
	- sudo gem install sass
	- sudo gem install compass
	- compass create // be sure to be in the project folder because the subfolders and files will be created

4- Node express-handlebars installation
	From terminal and being in the project folder
	
	- npm install express-handlebars
	- Create de subfolders
	    - mkdir views
	    - cd views
	    - mkdir layouts
	- Create app.js
	    - Copy app.js from other project
	    - Open with Sublime
	- Make some modifications:
		- defaultLayout: 'default'
		- render('mainContent')
		- in views folder:  touch mainContent.hbs
		- in views/layouts folder: touch default.hbs
		- in project folder: touch index.html

