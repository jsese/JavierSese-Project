const express = require('express');
const exphbs  = require('express-handlebars');
const app = express();
 
// Register Handlebars view engine
app.engine('.hbs', exphbs({defaultLayout: 'default', extname: '.hbs'}));

// Use Handlebars view engine
app.set('view engine', '.hbs');

// Ruta estática al proyecto
app.use(express.static('.'));
 
app.get('/', function (req, res) {
    res.render('mainContent');
});
 
app.listen(3000, function() {
	console.log('Opened server listening PORT 3000 -- localhost:3000')
});