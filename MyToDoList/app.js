// Create a new list item when clicking on the "Add" button
function newElement() {
    var inputValue = document.getElementById('myInput').value;
    var ul = document.getElementById('myUL');
    if(inputValue === ''){
        alert('You must write something');
    }else {
       ul.innerHTML += `<li>${inputValue}<span class="close">X</span></li>`;
    }
    var inputValue = document.getElementById('myInput').value = '';
    // Click on a close button to hide the current list item
    var close = document.getElementsByClassName('close');
    for(var i = 0; i<close.length; i++) {
        close[i].addEventListener('click', function() {
            var li = this.parentElement;
            li.style.display = 'none';
        });
    }
    // Add a "checked" symbol when clicking on a list item
    var items = document.getElementsByTagName('li');
    for(var i = 0; i<items.length; i++) {
        items[i].addEventListener('click', function() {
            this.classList.toggle('checked');
        });
    }
}
