Deberes lunes 25 de junio.

Con el plugin Jquery Validation hacer un formulario que valide:

- número de móvil, que tenga 9 dígitos
- el nombre tenga entre 2 y 20 caracteres
- el mail contenga el caracter @ (que sea una dirección de correo válida)

Los tres campos son obligatorios

Se utiliza Jquery, Jquery.validate y SASS

Jquery.validate tiene como elementos "rules" y "messages"
"rules" define las reglas que debe seguir cada campo del formulario.
"messages" define los mensajes de error para cada regla que no se cumpla

