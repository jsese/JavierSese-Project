$(function() {

  $("form[name='formulario']").validate({
    // Reglas (rules)
    rules: {

      // regla para el móvil: obligatorio, longitud 9, solo dígitos
      movil: {
        required: true,
        minlength: 9,
        maxlength: 9,
        digits: true
      },

      // regla para el nombre: obligatorio, entre 2 y 20 caracteres
      nombre: {
        required: true,
        minlength: 2,
        maxlength: 20
      },

      // regla para el email: obligatorio, dirección de correo válida
      email: {
        required: true,
        email: true
      }
    },

    // Mensajes de error
    messages: {

      movil: {
        required: "Introduzca su número de móvil",
        minlength: "Su número de móvil ha de tener 9 dígitos",
        maxlength: "Su número de móvil ha de tener 9 dígitos",
        digits: "Su número de móvil solo puede tener dígitos numéricos"
      },

      nombre: {
        required: "Introduzca su nombre",
        minlength: "Su nombre debe tener 2 ó más caracteres",
        maxlength: "Su nombre no puede tener más de 20 caracteres"
      },

      email: "Introduzca una dirección de correo válida"
    }
  });
});